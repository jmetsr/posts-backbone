class RootController < ApplicationController
  def root
    render :show
  end
end