class PostsController < ApplicationController
  def create
    @post = Post.new(self.post_params)
    if @post.save
      render json: @post
    else
      render json: @post.errors, :status => :unprocessable_entity
    end

  end
  def show
    @post = Post.find(params[:id])
    render json: @post
  end
  def index
     @posts = Post.all;
 #   render "index"
     @posts = Post.all
     render json: @posts
  end
  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    render json: {"this doesnt matter" => 3}
  end


  def post_params
    self.params[:post].permit(:title,:body)

  end

end