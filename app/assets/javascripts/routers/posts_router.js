Journal.Routers.PostsRouter = Backbone.Router.extend({
  routes: {
    "": "postsIndex",
    "posts/new": "PostsNew",
    "posts/:id": "PostShow"
  },


  postsIndex: function(){
    var c = new Journal.Collections.Post({});
    c.fetch({
      success: function(){
        var postIndex = new Journal.Views.PostsIndex({collection: c});
        postIndex.render();
        $('.posts').html(postIndex.$el);
      }
    })
  },

  PostShow: function(){
    var c = new Journal.Collections.Post({});
    c.fetch({
      success: function(){
        var postShow = new Journal.Views.PostShow({model: c.models[0]});
        postShow.render();
        $('.post').html(postShow.$el);
        console.log("postShow");
      }
    })
  },

  PostsNew: function(){
    var newPost = new Journal.Views.PostNew();
    var postForm = new Journal.Views.PostNew({model: newPost});
    postForm.render();
    console.log("posts new")
    console.log(postForm.$el);
    $('.newpost').html(postForm.$el);
  }
});