Journal.Views.PostShow = Backbone.View.extend({

  template: JST["posts/show"],

  render: function(){
    m = this.model;
    console.log(m);
    var templ = this.template({post: m});
    this.$el.html(templ);
    console.log(templ);
    return this;

  },

  initialize: function(options){
  },

  events: {
  },

})