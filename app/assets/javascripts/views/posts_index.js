Journal.Views.PostsIndex = Backbone.View.extend({

  template: JST["posts/index"],

  render: function(){
    c = this.collection;
    var templ = this.template({posts: this.collection});
    this.$el.html(templ);
    console.log("render",this.collection.length)
    return this;
  },

  initialize: function(options){
    this.listenTo(this.collection, "remove", this.render);
    this.listenTo(this.collection, "add", this.render);
    this.listenTo(this.collection, "change:title", this.render);
    this.listenTo(this.collection, "reset", this.render);
  },

  events: {
    "click .delete": "deletepost"
  },

  deletepost: function(){
    var id = $(event.target).data("id")
    this.collection.get(id).destroy()
  }
})